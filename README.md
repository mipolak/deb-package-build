# deb package build 

CICD pipeline template for building deb packages


## Requirements

### CICD Variables

- CI_VERSION     - package version number
- CI_ARCH        - package target architecture
- CI_DESC_LONG   - package long description

## Template usage

set CICD variables

place your files (sources) in root repository folder

update build section code in build_package script 

enable CICD pipeline
 
enjoy the build process :D
